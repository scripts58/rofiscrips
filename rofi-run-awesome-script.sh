#!/bin/bash

# Script name: rofi-run-awesome-script.sh
# Description: run scripts from the "~/.config/awesome/scripts" dir

# TODO: add option: return to upper dir

declare path_to_awesome_scrypts="$HOME/.config/awesome/scripts"

function rofi_select() {
  declare -a s=$options
  if [ $1 != $path_to_awesome_scrypts ]; then
    # options=( "${options[@]}")
    # declare -a s=$options
    # options=("..")
    # options+=( "${s[@]}" )
    # options=( "${s[@]}" )
    options=$(ls $1)
  else
    options=$(ls $1)
  fi

  selection=$(printf '%s\n' $options | rofi -dmenu)
  [ -z "$selection" ] && exit
  if [ -d "$1/$selection" ]; then
    echo "dir"
    rofi_select "$1/$selection"
  else
    declare arguments=$(rofi -dmenu -p "insert arguments")
    echo "c: $c"

    file_type=$(file $1/$selection)
    IFS=':' read -r -a a <<< $file_type
    file_type=${a[1]:1}
    echo $file_type
    case "$file_type" in
      *"Python script"*)
        python "$1/$selection"
      ;;
      *"shell script"*|*"ASCII text"*)
        c="$1/$selection $arguments"
        $c
      ;;
      *) exit 2
    esac
  fi
}

rofi_select $path_to_awesome_scrypts
