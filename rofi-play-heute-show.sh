#!/bin/bash

# Script name: rofi-play_heute_show.sh
# Description: rofi script for playing "heute-show"
# Dependencies: media player(mpv)

# Note: "heute-show" is a German TV-show

declare mediaplayer="mpv"

function get_url_from_date() {
  day=$1
  [ "${day:0:1}" == "0" ] && day=${day:1:1}
  month=$2
  [ "${month:0:1}" == "0" ] && month=${month:1:1}
  case "$month" in
    1) month="januar";;
    2) month="februar";;
    3) month="maerz";;
    4) month="april";;
    5) month="mai";;
    6) month="juni" ;;
    7) month="juli";;
    8) month="august";;
    9) month="september";;
    10) month="oktober";;
    11) month="november";;
    12) month="dezember";;
  esac
  year=$3
  echo "https://www.zdf.de/comedy/heute-show/heute-show-vom-$day-$month-$year-100.html"
}

function play_current() {
  declare -a dates=("$(date '+%d %m %Y')", "$(date -d "yesterday" '+%d %m %Y')", "$(date -d "2 day ago" '+%d %m %Y')")
  for date in "${dates[@]}"; do
    if curl --output /dev/null --silent --head --fail "$(get_url_from_date ${date[0]} ${date[1]} ${date[2]})"; then
      $mediaplayer "$(get_url_from_date ${date[0]} ${date[1]} ${date[2]})"
    elif [ "${date[@]}" == "${dates[-1]}" ]; then
      echo "ther is no current \"heute-show\""
    fi
  done
}

declare -a options=(
  "current"
  "from_date"
  "some_days_ogo"
  "last_friday"
)

function play_from_last_friday() {
    declare -a date=$(date --date="last Fri" '+%d %m %Y')
    $mediaplayer $(get_url_from_date ${date[0]} ${date[1]} ${date[2]})
}

function play_from_some_days_ogo() {
  if [ $1 == "1" ]; then
    declare -a date=$(date -d "yesterday" '+%d %m %Y')
  else
    declare -a date=$(date -d "$1 day ago" '+%d %m %Y')
  fi

  if curl --output /dev/null --silent --head --fail "$(get_url_from_date ${date[0]} ${date[1]} ${date[2]})"; then
    $mediaplayer $( get_url_from_date ${date[0]} ${date[1]} ${date[2]})
  else
    echo "no epesode has been released $1 day(s) ago on $(echo "${date[@]}" | tr " " ".")"
  fi
}


declare selected_option=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -p "play \"heute-show\"")

function rofi_from_date() {
  date=$(rofi -dmenu -p "insert date (e.g. 2 4 2021)")
  [ -z $date ] && exit
  if curl --output /dev/null --silent --head --fail "$(get_url_from_date ${date[0]} ${date[1]} ${date[2]})"; then
    $mediaplayer $( get_url_from_date ${date[0]} ${date[1]} ${date[2]})
  else
    echo "no epesode has been released on $(echo "${date[@]}" | tr " " ".")"
  fi
}

function rofi_some_days_ogo() {
  n=$(rofi -dmenu -p "insert how many days ago")
  [ -z $n ] && exit
  play_from_some_days_ogo $n
}

case "$selected_option" in
  "current") play_current ;;
  "from_date") rofi_from_date ;;
  "some_days_ogo") rofi_some_days_ogo ;;
  "last_friday") play_from_last_friday ;;
  *) exit ;;
esac
