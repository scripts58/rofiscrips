# rofiscrips

<p> This is a collection of rofi scripts <\p>

## NOTE:
<p>
If you wont to use this scripts you will probably need to adjust some of the paths. </p>
<p>
Furthermore, some scripts read out my preferred terminal and editor. I set them with a script that you can find under: https://gitlab.com/pxxx/dotfiles/-/blob/main/.config/sxhkd/variables.
Alternatively you can just replace them with your own values.

## TODO:

- add a script to convert rofi scripts to dmenu scripts
- complete rofi-xrandr.sh
- complete rofi-resulution.sh
- complete rofi-theme.sh
