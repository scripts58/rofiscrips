prozess="$(ps -e | tail -n+2 | rofi -dmenu -i)"
[ -z "$prozess" ] && exit
kill $(echo "$prozess" | awk '{print $1}')
