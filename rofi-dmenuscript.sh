#!/bin/bash

# Script name: rofi-dmenuscript.sh
# Description: run dmenuscript
# Dependencies: rofi, dmenu

cd || exit 2
path="./.dmenu/"
choice=$(ls "$path" | rofi -dmenu -i -p "dmenuscript")
echo "path: $path"
echo "choice: $choice"
command="$path$choice"
echo "command: $command"
[ -z "$choice" ] && exit
"$command"
