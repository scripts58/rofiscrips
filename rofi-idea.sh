#!/bin/bash

######################################################################
# @author       : pxxx
# @created      : 04.12.2021
#
# @description  : open idea project inside of idea
######################################################################

workspaces=(
  "$HOME/IdeaProjects/"
  "$HOME/Workspaces/IntelliJWorkspace/"
  "$HOME/IntelliJProjects/"
)

declare -a existingWorkspaces
for e in ${workspaces[@]}; do
  if [ -d "$e" ]; then
    existingWorkspaces+=("$e")
  fi
done

function p() {
  content=("$(ls $1)")
  for e in content; do
    printf "$1%b\n" ${content[@]} | sed -e "s!$HOME!~!"
  done
}

project="$(for e in "${existingWorkspaces[@]}"; do p "$e"; done | rofi -dmenu -i -p "select project")"
[ -z "$project" ] && exit
cmd="idea $project"
echo $cmd
# FIX: lauch
# $cmd
# idea "$project"
# idea $project
