!/bin/bash

function getTerminalFromBashrc() {
  IFS='"' read -r -a terminal <<< "$( grep "^export TERMINAL" ~/.bashrc )"
  echo "${terminal[-1]}"
}

terminal="$(~/.config/sxhkd/variables terminal1)"

# I set the title to maximized in order to maximize the window with my awesomeWM configuration
declare editor_command="$terminal --title maximized -e $EDITOR"

declare -a options=(
"$HOME/.Xresources"
"$HOME/.autostart.sh"
"$HOME/.bashrc"
"$HOME/.config/alacritty/alacritty.yml"
"$HOME/.config/awesome/rc.lua"
"$HOME/.config/awesome/window_rules.lua"
"$HOME/.config/broot/conf.toml"
"$HOME/.config/bspwm/bspwmrc"
"$HOME/.config/dunst/dunstrc"
"$HOME/.config/herbstluftwm/autostart"
"$HOME/.config/i3/config"
"$HOME/.config/kitty/kitty.conf"
"$HOME/.config/nvim/init.lua"
"$HOME/.config/nvim/init.vim"
"$HOME/.config/nvim/vim-plug/plugins.vim"
"$HOME/.config/picom/picom.conf"
"$HOME/.config/polybar/config"
"$HOME/.config/qtile/config.py"
"$HOME/.config/qutebrowser/autoconfig.yml"
"$HOME/.config/qutebrowser/quickmarks"
"$HOME/.config/stumpwm/config"
"$HOME/.config/sxhkd/sxhkdrc"
"$HOME/.config/termite/config"
"$HOME/.config/vifm/vifmrc"
"$HOME/.config/vimb/config"
"$HOME/.config/xmobar/xmobarrc2"
"$HOME/.dmenu/"
"$HOME/.doom.d/config.el"
"$HOME/.doom.d/init.el"
"$HOME/.emacs.d/init.el"
"$HOME/.rofi/"
"$HOME/.spectrwm.conf"
"$HOME/.tmux.conf"
"$HOME/.vimrc"
"$HOME/.xmonad/xmonad.hs"
"$HOME/.zshrc"
)

declare -a existingoptions
for e in ${options[@]}; do
  if [ -e "$e"  ]; then
    existingoptions=(${existingoptions[@]} "$e")
  fi
done

choice=$(printf '%s\n' "${existingoptions[@]}" | rofi -dmenu -p 'Edit config file')
[ -z "$choice" ] && exit

while [ -d "$choice" ]; do
  choice=$(echo "$(tree -fiL 1 "$choice")" | rofi -dmenu -p 'Edit config file')
  [ -z "$choice" ] && exit
done

c="$editor_command $choice"
echo "$c"
$c
