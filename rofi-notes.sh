#!/bin/bash

# Script name: rofi-note.sh
# Description: edit notes
# Dependencies: rofi, trash

note_path="$HOME/Documents/notes/"


# --{{
# I set:
# export EDITOR="nvim"
# and:
# export TERMINAL="kitty"
# in my bashrc. The getTerminalFromBashrc and getEditorFromBashrc functions grep the values from there.
# you can just set the terminal and the editor variable manual if you want.

function getTerminalFromBashrc() {
  IFS='"' read -r -a terminal <<< "$( grep "^export TERMINAL" ~/.bashrc )"
  echo "${terminal[-1]}"
}

terminal="$(~/.config/sxhkd/variables terminal1)"
# editor="$(~/.config/sxhkd/variables cmd_editor)"
# editor="$EDITOR"
# editor="nvim"
editor="$HOME/.local/bin/lvim"


known_editor_fallback=("$EDITOR" nvim vim vi mousepad nano)
if ! command -v "$editor" &>/dev/null; then
  for ed in "${$known_editor_fallback[@]}"; do
    if command -v "$ed" &>/dev/null; then
      editor="$ed"
    fi
  done
fi
# --}}

# I set the title to maximized in order to maximize the window with my awesomeWM configuration
# declare editor_command="$terminal --title maximized -e $editor"
declare editor_command="$terminal -e $editor"
# declare filemanager_command="$terminal --title maximized -e $HOME/.bashrc && ~/.config/vifm/scripts/vifmrun"
declare filemanager_command="$terminal --title maximized -e $HOME/.config/vifm/scripts/vifmrun"

function create() {
  name=$(rofi -dmenu -p "create")
  [ -z "$name" ] && exit
  touch "$note_path$name"
}

function delite() {
  selection=$(find "$note_path" | rofi -dmenu -i -p "delite")
  [ -z "$selection" ] && exit
  trash "$note_path$selection" # put file into ~/.local/share/Trash/files
  # rm "$note_path$selection"
}

function getIntArryFromString () {
  declare -a array
  if [[ "$1" == *"-"* ]] && [[ "$1" == *","* ]]; then
      IFS="," read -r -a split <<< "$1"
      for i in "${split[@]}"; do
        array+=($(getIntArryFromString "$i"))
      done
  elif [[ "$1" == *"-"* ]]; then
    declare seqStart
    declare seqEnd
    IFS="-" read -r -a split <<< "$1"
    seqStart=${split[0]}
    seqEnd=${split[1]}
    array=($(seq "$seqStart" "$seqEnd"))
  elif [[ "$1" == *","* ]]; then
      IFS="," read -r -a split <<< "$1"
      for i in "${split[@]}"; do
        array+=("$i")
      done
  else
    array="$1"
  fi

  echo -e ${array[@]}
}

function getHeadlines() {
  declare headlineSearchPattern
  declare headlineLevel

  declare -a arry
  if [[ "$1" == "a" ]]; then
    array=($(getIntArryFromString "0-9"))
  else
    array=($(getIntArryFromString "$1"))
  fi

  headlineSearchPattern="#>${array[0]}#"
  for i in ${array[@]:1}; do
    headlineSearchPattern+="\|#>$i#"
  done

  grep -r -n "$headlineSearchPattern" $([ -z $2 ] && echo "*" || echo ${@:2})
}

function searchForHeadline () {
  level=$(rofi -dmenu -p "enter headline level(also accepts a range(e.g '3-4'), list(e.g '0,3,5') or 'a' for all)")
  [ -z "$level" ] && level="a"
  l=$(getHeadlines "$level" | rofi -dmenu -i)
  [ -z "$l" ] && select_a
  command="$editor_command $(echo "$l" | awk -F ':' '{print $1}')"
  [[ "$editor_command" == *"vim"* ]] && command+=" +$(echo "$l" | awk -F ':' '{print $2}')"
}

function search() {
  seach_pettern=$(rofi -dmenu -i -p "seach pettern")
  [ -z "$seach_pettern" ] && select_a
  results=$(grep -r -n "$seach_pettern" *)
  selection=$(echo "${results[@]}" | rofi -dmenu -i -p "select")
  [ -z "$selection" ] && select_a && return
  IFS=":" read -r -a selection <<< "$selection"
  if [[ "$editor_command" == *"vim"* ]]; then
    command="$editor_command +/$seach_pettern +${selection[1]} ${selection[0]}"
  else
    command="$editor_command ${selection[0]}"
  fi
}

function select_a () {
  options=(
  "1.create"
  "2.delite"
  "3.search"
  "4.search_for_head_line"
  "5.open_in_file_manager"
  "-----------------------------------------------------------"
  "$(ls "$note_path")"
  )
  selection=$(printf '%s\n' "${options[@]}" | rofi -dmenu -i -p "select note")
  case "$selection" in
    "") exit ;;
    "-----------------------------------------------------------") select_a ;;
    "${options[0]}")  create "" && select_a ;;
    "${options[1]}")  delite "" && select_a ;;
    "${options[2]}")  search ;;
    "${options[3]}")  searchForHeadline ;;
    "${options[4]}")  command="$filemanager_command $note_path" ;;
    *)                command="$editor_command $note_path$selection" ;;
  esac
}

cd "$note_path" || (echo "$note_path is not a dir"  && exit 1)
select_a
echo "command: $command"
$command
