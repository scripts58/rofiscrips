#!/bin/bash

# Script name:  rofi-runsh.sh
# Description:  run a set of shell commands (I declarate
#               some shortcuts for commands) This is a script to run commands that I
#               personally run often. You can of cause add your own commands.
# Dependencies: rofi

terminal="$(~/.config/sxhkd/variables terminal1)"


function run-cmd {
  command="$(rofi -dmenu -p "$1")"
  [ -z "$command" ] && exit

  function set_brightness () {
  declare options="$(xrandr --listmonitors)"
  options="${options:12}"
  declare i=0
  for e in $options; do
    i=$[i+1]
    if [ $[i%4] == 0 ]; then
      command="xrandr --output $e --brightness $1"
      echo "command: $command"
      $command
    fi
  done
  }

  case "$command" in
    "a") ~/.autostart.sh ;;
    "r") reboot ;;
    "s" | "suspend") systemctl suspent ;;
    "h" | "hibernate") systemctl hibernate ;;
    "hs" | "hybrid-sleep") systemctl hybrid-sleep ;;
    "p") poweroff ;;

    "hy" | "history") # ~/.rofi/rofi-bash_history.sh ;;
      cmd=$(tac ~/.bash_history | rofi -dmenu -p "history")
      [ -z "$cmd" ] && exit
      echo $"$cmd"
      $cmd
    ;;
    "rk" | "reloadKeybord") .config/keyboard-settings.sh ;;
    "nr") nitrogen --restore ;;
    "hb") hsetroot ;;
    "hr")
        curentPicture="$HOME/Pictures/wallpaper/hsetroot/currentWallpaper"
        optionFile="$HOME/Pictures/wallpaper/hsetroot/option"
        hsetroot $(cat "$optionFile") "$curentPicture"
      ;;
    "cw")
      ~/.scripts/wallpapersetter.sh ~/Pictures/wallpaper/anime/cyber_punk/b/
      ;;
    sb*)
      IFS=' ' read -ra a <<< "$command"
      set_brightness "${a[-1]}"
    ;;
    *) $command | run-cmd "command not found";;
  esac
}

run-cmd "run cmd"
