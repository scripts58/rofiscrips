#!/bin/bash

# Script name: rofi-select-screenlayout.sh
# Description: select screenlayout
# Dependencies: rofi, xrandr(for the screenlayout-files)

path="$HOME/.screenlayout/"
choise=$(ls "$path" | rofi -dmenu -i -p "select screenlayout")
case "$choise" in
  "") exit ;;
  *) "$path$choise" ;;
esac
