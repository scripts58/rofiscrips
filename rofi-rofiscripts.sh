#!/bin/bash

# Script name: rofi-rofiscripts.sh
# Description: run rofiscripts
# Dependencies: rofi

# rofiPath="$HOME/.rofi/"
rofiPath="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

declare -a blackList=(
  "README.md"
  "convertRofiToDmenu.sh"
)

cd "$rofiPath" || {
  echo "path to rofi scripts: \"$rofiPath\" no directory" >&2
  exit
}

selection=$(printf '%s\n' $(ls) | grep -v -x "$(printf '%s\|' ${blackList[@]})" | rofi -dmenu -i -p "select rofiscript")
[ -z "$selection" ] && exit
cmd="./$selection"
echo "cmd: $com"
"$cmd"
