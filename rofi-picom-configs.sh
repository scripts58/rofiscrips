#!/bin/bash

# Script name: rofi-picom-configs.sh
# Description: select picom-profile
# Dependencies:  rofi, picom

parth="$HOME/.config/awesome/picom/"
echo "select profile from: $parth"

parth+=$(ls "$parth" | rofi -dmenu -i -p "select picom-profile" )
echo "parth: $parth"

exec picom --config "$parth"
