#!/bin/bash

# Script name: rofi-rofiscripts-dmenuscripts.sh
# Description: run rofiscripts or dmenuscripts
# Dependencies: rofi, dmenu

cd ~
category=$(echo -e "rofi\ndmenu" | rofi -dmenu -i -p "rofiscripts/dmenuscripts" )

case "$category" in
  "rofi")
    selection=$(ls ./.rofi | rofi -dmenu -i )
    [ -z "$selection" ] && exit
    com="./.rofi/"$selection
    echo "com: $com"
    "$com"
  ;;
  "dmenu")
    path="./.dmenu/"
    choice=$(ls $path | rofi -dmenu -i -p "dmenuscript")
    echo "path: $path"
    echo "choice: $choice"
    command="$path$choice"
    echo "command: $command"
    [ -z "$choice" ] && exit
    "$command"
  ;;
  *) exit ;;
esac
