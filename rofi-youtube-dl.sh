#!/bin/bash

# Script name: rofi-youtube-dl.sh
# Description: download youtube videos whith youtube-dl
# Dependencies: rofi, youtube-dl

function changeDir {
  case "$1" in
    "") exit ;;
    "\\select/" ) return ;;
    "\\new_dir/" )
      new_dir_name="$(rofi -dmenu -i -p "name")"
      [ -n "$new_dir_name" ] && mkdir "$new_dir_name" ;;
    *) cd "$1" || exit 1 ;;
  esac
  declare -a d=("$(find ./ -maxdepth 1 -follow -type d | sort -t '\0' -n)")
  d=( "\\select/" "\\new_dir/" ".." "${d:2}")
  changeDir "$(printf '%s\n' "${d[@]}" | rofi -dmenu -p "$(pwd)")"
}

changeDir "$HOME"

formate=$(echo -e "mp3\nmp4" | rofi -dmenu -i -p "choose formate")
[ -z "$formate" ] && exit
url=$(rofi -dmenu -i -p "url")
[ -z "$url" ] && exit

case "$formate" in
  "mp3") youtube-dl -x --audio-format mp3 -ciw -o "%(title)s.%(exit)s" "$url" ;;
  # "mp4") youtube-dl -x --recode-video mp4 -ciw -o "%(title)s.%(exit)s" "$url" ;;
  "mp4") youtube-dl -x --recode-video mp4 "$url" ;;
      *) exit 1;;
esac
