#!/bin/bash
#  ____ _____
# |  _ \_   _|  Derek Taylor (DistroTube)
# | | | || |    http://www.youtube.com/c/DistroTube
# | |_| || |    http://www.gitlab.com/dwt1/
# |____/ |_|
#
# Dmenu script for launching system monitoring programs.

# Note: I just changed it a litle bit


# --{{
# I set:
# export TERMINAL="kitty"
# in my bashrc. The getTerminalFromBashrc function grep the value from there.
# You can just set the terminal and the editor variable manual if you want.

terminal="$(~/.config/sxhkd/variables terminal1)"
# --}}

declare -a options=( bpytop btop bashtop glances gtop htop iftop iotop iptraf-ng ncdu nmon s-tui)

declare -a installed=()
for o in ${options[@]}; do
  if command -v "$o"  &>/dev/null; then
    installed+=("$o")
  fi
done

choice=$(printf '%b\n' "${installed[@]}" | rofi -dmenu -i -p 'System monitors')

[[ -z "$choice" ]] && exit # exit if nothing is chosen

case $choice in
  # declare special behaviour for certain programs
  iftop| \
  iotop| \
  iptraf-ng) $terminal --hold -e gksu $choice ;;

  # default behaviour
  *) $terminal -e $choice ;;
esac
