#!/bin/bash

#XXX:in progress

# NOTE: In order to get this script running you need to have the right file structure
#TODO: Explain the right structure for each program

declare -A theme_dirs=(
  ["awesomewm"]="$HOME/.config/awesome/themes/"
  ["rofi"]="$HOME/.config/rofi/themes/"
  ["alacritty"]="$HOME/.config/alacritty/"
  ["vifm"]="$HOME/.config/vifm/theme/colors/"
  ["gtk"]="$HOME/.themes/"
)

function set_awesomewm_theme() {
  [ -z "$1" ] && exit
  # NOTE: I know ln -sf should overwrite the link witch would make the following line redundant but for what ever reason it doesn't work
  if [ -e "${theme_dirs["awesomewm"]}current" ]; then
    trash "${theme_dirs["awesomewm"]}current"
  fi
  ln -s $1 ${theme_dirs["awesomewm"]}current
}

function select_awesomewm_theme() {
  declare -a blackList=("current")
  set_awesomewm_theme "$(ls ${theme_dirs["awesomewm"]}|grep -v ${blackList[@]}|rofi -dmenu -i -p 'select awesomewm theme')"
}

function set_gtk_theme() {
  [ -z "$1" ] && exit
  # NOTE: I know ln -sf should overwrite the link witch would make the following line redundant but for what ever reason it doesn't work
  if [ -e "${theme_dirs["gtk"]}current" ]; then
    trash "${theme_dirs["gtk"]}current"
  fi
  ln -s $1 ${theme_dirs["gtk"]}current
}

function select_gtk_theme() {
  declare -a blackList=("current")
  set_gtk_theme "$(ls ${theme_dirs["gtk"]}|grep -v ${blackList[@]}|rofi -dmenu -i -p 'select gtk theme')"
}

function set_rofi_theme() {
  [ -z "$1" ] && exit
  # NOTE: I know ln -sf should overwrite the link witch would make the following line redundant but for what ever reason it doesn't work
  if [ -e "${theme_dirs["rofi"]}current.rasi" ]; then
    trash "${theme_dirs["rofi"]}current.rasi"
  fi
  ln -s "$1.rasi" "${theme_dirs["rofi"]}current.rasi"
}

function select_rofi_theme() {
  declare -a blackList=("current")
  set_rofi_theme "$(ls ${theme_dirs["rofi"]}|sed 's/\.rasi//g' |grep -v ${blackList[@]}|rofi -dmenu -i -p 'select rofi theme')"
}


function set_alacritty_theme() {
  [ -z "$1" ] && exit
  # NOTE: I know ln -sf should overwrite the link witch would make the following line redundant but for what ever reason it doesn't work
  if [ -e "${theme_dirs["alacritty"]}alacritty.yml" ]; then
    trash "${theme_dirs["alacritty"]}alacritty.yml"
  fi
  ln -s "$1.yml" "${theme_dirs["alacritty"]}alacritty.yml"
}

function select_alacritty_theme() {
  declare -a blackList=("current")
  set_alacritty_theme "$(ls ${theme_dirs["alacritty"]}|sed 's/\.yml//g' |grep -v ${blackList[@]}|rofi -dmenu -i -p 'select alacritty theme')"
}

function set_vifm_theme() {
  [ -z "$1" ] && exit
  # NOTE: I know ln -sf should overwrite the link witch would make the following line redundant but for what ever reason it doesn't work
  if [ -e "${theme_dirs["vifm"]}current" ]; then
    trash "${theme_dirs["vifm"]}current"
  fi
  ln -s "$1.vifm" "${theme_dirs["vifm"]}current"
}

function select_vifm_theme() {
  declare -a blackList=("current")
  set_vifm_theme "$(ls ${theme_dirs["vifm"]}|sed 's/\.vifm//g' |grep -v ${blackList[@]}|rofi -dmenu -i -p 'select vifm theme')"
}

function select_program() {
  selection="$(printf '%b\n' ${!theme_dirs[@]} | rofi -dmenu -i -p "")"
  [ -z "$selection" ] && exit
  "select_${selection}_theme"
}

select_program
