#!/bin/bash

# Script name: rofi-media-control.sh
# Description: control media via rofi
# Dependencies: rofi, playerctl

declare -a options=(
"play/pause"
"next"
"previous"
"stop"
)
chose=$(printf '%s\n' "${options[@]}" | rofi -dmenu -p "media-control")

case "$chose" in
  "play/pause") playerctl play-pause:w ;;
  "next") playerctl next ;;
  "previous") playerctl previous ;;
  "stop") playerctl stop ;;
  *) exit ;;
esac

