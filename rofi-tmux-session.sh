
# Description: open terminal and attach it to selected tmux session
# NOTE: session-names are not allowed to contain spaces

function getTerminalFromBashrc() {
  IFS='"' read -r -a terminal <<< "$( grep "^\s*export TERMINAL" ~/.bashrc )"
  echo "${terminal[-1]}"
}
session=$(tmux list-sessions | rofi -dmenu -i | awk -F: '{print $1}')
[ -z "$session" ] && exit

declare cmd
allSesstions=($(tmux list-sessions | awk -F: '{print $1}'))
for ((i=0; i<="${#allSesstions[@]}"; i++)); do
  if [[ "${allSesstions[i]}" == "$session" ]]; then
    cmd="$(getTerminalFromBashrc) -e tmux attach-session -t $session"
    break
  elif [[ $i == "${#allSesstions[@]}" ]]; then
    cmd="$(getTerminalFromBashrc) -e tmux new-session -s $session"
  fi
done

echo "cmd=$cmd"
$cmd
