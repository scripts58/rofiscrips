#!/bin/bash

# just show all the installed packages


o=$(paru -Qi)
n=("$(echo "$o" | grep "^Name" | awk -F ':' '{print $NF}')")
# d=("$(echo "$o" | grep "^Description" | awk -F ':' '{print $NF}')")

# declare -a result
# for (( i=0; i<${#n[@]}; ++i)); do
#    result+=( "${n[$i]}" "${d[$i]}" )
# done

printf '%b\n' ${n[@]} | rofi -dmenu

# paru -Qi | rofi -dmenu
