#!/bin/bash

# Script name: rofi-brightness.sh
# Description: change monitor brightness
# Dependencies: rofi, xrandr

function set_brightness () {
  command="xrandr --output $1 --brightness $2"
  echo "command: $command"
  $command
}

declare -a options=($(xrandr --listmonitors | tail -n+2 | awk '{print $NF}' | xargs echo))

[ ${#options[@]} -gt 1 ] && options=("all" ${options[@]})
echo "${options[@]}"

selection=$(printf '%s\n' "${options[@]}" | rofi -i -dmenu -p "select montor")
[ -z "$selection" ] && exit

brightness=$(rofi -dmenu -p "brightness")
[ -z "$brightness" ] && exit

if [ "$selection" != "all" ]; then
  set_brightness "$selection" "$brightness"
else
  for e in "${options[@]}" ; do
    set_brightness "$e" "$brightness"
  done
fi
