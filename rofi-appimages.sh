#!/bin/bash

# Script name:  rofi-appimages.sh
# Description:  launch appimages files from the
#               ~/Applications-folder.
#               Shows if it's not executable and if
#               you select a not executable
#               appimage it will ask you if you wont
#               to make it executable
# Dependencies: rofi

declare path="$HOME/Applications/"

while true; do

  choice=$( printf '%s\n' "$(ls -l "$path"| tail -n+2 | awk '{print $1" "$NF}' | cut -c 10-)" | rofi -dmenu -i -p "Appimage")
  [ -z "$choice" ] && exit

  if [ $(echo  "$choice" | awk '{print $1}') == "-" ]; then
    [ $(echo -e "yes\nno" | rofi -dmenu -i -p "make executable?") == "yes" ] && chmod +x "$path$(echo "$choice" | awk '{print $2}')"
  else
    "$path$(echo "$choice" | awk '{print $2}')"
    exit
  fi
done
