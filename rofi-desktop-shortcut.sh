#!/bin/bash

# Description: With this you can launch desktop shortcuts

cd "$HOME/Desktop" || (echo "~/Desktop doesn't exist" & exit 1)
d=`ls ./ | rofi -dmenu -i -p "run"`
[ -z "$d" ] && exit
`grep "Exec" "$d" | awk -F= '{print $2}'`
